# Burrow Extension For Chrome

Burrow allows you to access Gopher sites using Chrome. Gopher is the predecessor of the World Wide Web and HTTP, so Gopher sites look very "dated" to today's users. However, Gopher provides a much simpler, cleaner browsing experience, and many former Gopher users fondly remember the Gopher web.

There is still a community of Gopher enthusiasts who continue to run servers and sites, but it can be hard to access these in standard browsers. Google Chrome, for instance, does not offer a native Gopher client. Burrow attempts to make Gopher access and navigation possible for Chrome users.

## How does it work?

Currently this extension rewrites all gopher:// links to point to Floodgap's Public Gopher Proxy. It relies on Javascript to accomplish this, so make sure that Javascript is enabled!

Eventually I hope to build in a local Gopher client proxy using TCP/IP.

## Licensing and contributions

Burrow is released freely under the MIT license.

Pull requests and other contributions are welcome. By submitting a pull request for this project, you agree to license your contribution under the MIT license to this project.